variable "bucket_name_suffix" {
  type        = string
  description = "(optional) suffix to append to the bucket name"
  default     = "1"
}

# Adding this comment so we can have a 0.0.2

resource "aws_s3_bucket" "this" {
  bucket = "hi-renovate-devs-${var.bucket_name_suffix}"
  acl    = "private"
}
